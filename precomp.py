import argparse
from sys import argv, platform
import subprocess
from pathlib import Path
import re
import time

if platform == "NT":
	temp_exec = "./.prec.exe"
else:
	temp_exec = "./.prec"

pattern = re.compile(r'#include "([a-zA-Z0-9_\-]+)#gen.h"')

template = """
/********************************************************|
|*              Generated with precomp.py               *|
|* https://gitlab.com/team-programming-socks/TPSScripts *|
|********************************************************/
{content}
""".strip()


parser = argparse.ArgumentParser()
parser.add_argument("input",action="append",type=Path)
parser.add_argument("--compiler","-c",default="c++")
parser.add_argument("--standalone","-s",action="store_true")
parser.add_argument("--threads","-t",default=6)
parser.add_argument("--force","-f",action="store_true")

def main():
	opts = parser.parse_args(argv[1:])

	inp: Path
	
	for inp in opts.input:
		print(f"working on {inp}...")
		with inp.open() as file:
			code: str = file.read()
		out: list[str] = []
		for line in code.splitlines():
			m = pattern.match(line)
			if m is None:
				if opts.standalone:
					out.append(line)
				continue
			print(f"{inp}: {m.group(1)}...")
			ofile = (inp.parent/f"{m.group(1)}#gen.h")
			sources = list(inp.parent.glob(f"{m.group(1)}.*"))
			if sources == []:
				print("couldnt find valid source file!")
			source = sources[0]
			print(f"using {source}...")

			if not opts.force and ofile.exists() and source.stat().st_mtime < ofile.stat().st_mtime:
				print("Found up to date file from previous run!")
				if opts.standalone:
					with ofile.open() as f:
						out.extend(f.readlines())
				continue
			status = subprocess.call(
				[
					opts.compiler, "-o", temp_exec, source,
					"-DPREC" if opts.standalone else "-DPREC -DPREC_DEV",
					f"-DPREC_THREADS={opts.threads}"
				])
			if status != 0:
				print(f"build failed! ({status})")
				continue
			print(f"built, running...")
			tstart = time.time()
			
			try:
				prec = subprocess.run([temp_exec],stdout=subprocess.PIPE)
			except KeyboardInterrupt:
				print("\nInterupted!")
				return

			if opts.standalone:
				out.extend(str(prec.stdout,"utf-8").splitlines())
			
			ofile.write_text(template.format(content=str(prec.stdout,"utf-8")))
			
			tdelta = time.time() - tstart
			print(f"\nPrecalculation finnished. Took {tdelta//60}m {tdelta%60:.2f}s ")
		if opts.standalone:
			print("writting...")
			with (inp.parent/(inp.stem+"#standalone"+inp.suffix)).open("w") as file:
				file.write(template.format(content="\n".join(out)))
	if Path(temp_exec).exists():
		Path(temp_exec).unlink()

if __name__ == "__main__":
	main()
