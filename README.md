# Scripts made by members of the programming socks team for use in competitive programming

## precomp.py

A utility script for pre-computing values and including them in c/c++ source files.

### Usage

`python precomp.py [file]`

In the file you want to use precomputed values, include `"<precomputing source code>#gen.h"`.\
Inside the pre-computing source code you are given a few macros defined by the script:

- `PREC` always defined
- `PREC_DEV` defined if building without the standalone flag
- `PREC_THREADS` number of threads to use

Flags/options:

- `-c`, `--compiler` the compiler to use
- `-s`, `--standalone` merge the source code and generated output into a single file
- `-t=N`, `--threads=N` number of threads to use (if supported by the pre-computing source file)
- `-f`, `--force` regenerate every precomputed file, even if it is newer than the source file.

